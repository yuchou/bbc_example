# django基本操作
```shell
virtualenv .venv
django-admin startproject bbc_example .
python manage.py migrate
python manage.py createsuperuser
django-admin startapp example
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```
# 集成xadmin
## 安装xadmin库
```shell
pip install git+git://github.com/sshwsfc/xadmin.git
```

## 安装xadmin apps
```python
INSTALLED_APPS = [
    'crispy_forms',
    'xadmin',
]
```

## 配置urls.py
```python
from django.conf.urls import url
import xadmin


urlpatterns = [
    url(r'^xadmin/', xadmin.site.urls),
]
```
## 定制adminx
```python
from example.models import Episodes, Catalogs
from xadmin import views
import xadmin


# 更换主题
# class BaseSetting(object):
#     enable_themes = True
#     use_bootswatch = True

# 全局配置
class GlobalSettings(object):
    site_title = "音频数据管理后台"
    site_footer = "by yuchou"

# 显示所有列
class EpisodesAdmin(object):
    # list_display = ['episode_id', 'title', 'subtitle', 'catalog', 'text', 'link', 'mp3_url', 'mp3_path', 'mp3_sum',
    #                 'pdf_url', 'pdf_path', 'pdf_sum', 'image_url', 'image_path', 'image_sum','add_time']
    list_display  = [f.name for f in Episodes._meta.fields]
    list_filter = ('episode_id', 'title', 'subtitle')
    search_fields = ['episode_id']

# 显示所有列
class CatalogsAdmin(object):
    list_display = [f.name for f in Catalogs._meta.fields]
    # list_display = ['name', 'url', 'add_time']
    list_filter = ('name', 'link')
    search_fields = ['name']


xadmin.site.register(Episodes, EpisodesAdmin)
xadmin.site.register(Catalogs, CatalogsAdmin)
xadmin.site.register(views.CommAdminView, GlobalSettings)
```

# django-rdf
## 安装依赖
```shell
pip install djangorestframework
pip install markdown 
pip install django-filter
```
## 安装rest-framework
```python
INSTALLED_APPS = [
    'rest_framework',
]
```

# 集成scrapy
## 创建bbcbots/__init__.py
```python
# bbcbots/__init__.py
def setup_django_env():
    import os, django

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bbc_example.settings")
    django.setup()


def check_db_connection():
    from django.db import connection

    if connection.connection:
        if not connection.is_usable():
            connection.close()
```
## 新建scapy项目
```shell
$ scrapy startproject bbcbot .
New Scrapy project 'bbcbot', using template directory '/Users/yuchou/gitlab/bbc_example/.venv/lib/python3.6/site-packages/scrapy/templates/project', created in:
    /Users/yuchou/gitlab/bbc_example/bbcbots

You can start your first spider with:
    cd .
    scrapy genspider example example.com
$ scrapy list
$ scrapy crawl bots
```

# 媒体文件访问配置
## bbc_example/settings.py
```python
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
```
## bbc_example/urls.py
```python
from django.conf.urls import url, include
from bbc_example.settings import MEDIA_ROOT
from django.views.static import serve
urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^media/(?P<path>.*)$', serve, {"document_root": MEDIA_ROOT}),
]
```
## example/models.py
```python
from django.db import models
from django.utils import timezone


class Episodes(models.Model):
    episode_id = models.CharField(max_length=255, verbose_name='编号')
    title = models.CharField(max_length=255, verbose_name='标题')
    subtitle = models.CharField(max_length=255, verbose_name='副标题')
    catalog = models.ForeignKey(Catalogs, related_name='episodes', verbose_name='类别')
    text = models.TextField(verbose_name='内容')
    link = models.URLField(verbose_name='网页链接')
    mp3_url = models.URLField(verbose_name='MP3下载链接')
    mp3_path = models.FileField(verbose_name='MP3本地路径')
    mp3_sum = models.CharField(max_length=255, verbose_name='MP3校验值')
    pdf_url = models.URLField(verbose_name='PDF下载链接')
    pdf_path = models.FileField(verbose_name='PDF本地路径')
    pdf_sum = models.CharField(max_length=255, verbose_name='PDF校验值')
    image_url = models.URLField(verbose_name='Image下载链接')
    image_path = models.ImageField(verbose_name='Image本地路径')
    image_sum = models.CharField(max_length=255, verbose_name='Image校验值')
    add_time = models.DateTimeField(default=timezone.now, verbose_name='添加时间')
```

# 添加分页功能
## 全局配置bbc_example/setting.py
```python
REST_FRAMEWORK = {
    # 全局分页配置
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 10
}
```
## 定制views分页
```python

```

# 添加过滤器
## 安装django-filter
```shell
pip install django-filter
```

## bbc_example/setting.py
```python
INSTALLED_APPS = [
    'django_filters',
]

REST_FRAMEWORK = {
    # 过滤器配置
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',),
}
```

## example/views.py
```python
from rest_framework import viewsets
from rest_framework import filters  # 搜索库
from django_filters.rest_framework import DjangoFilterBackend   # 过滤器库
from example.models import Episodes, Catalogs
from example.serializers import EpisodesSerializer, CatalogsSerializer


class EpisodesViewSet(viewsets.ModelViewSet):
    queryset = Episodes.objects.all()
    serializer_class = EpisodesSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('episode_id', 'title', 'catalog', 'link')      # 过滤配置
    search_fields = ('episode_id', 'title', 'catalog__name',)       # 搜索配置，外键使用field__field
    ordering_fields = ('episode_id', 'title', 'catalog__name',)     # 排序配置
```
