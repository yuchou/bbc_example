#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/5/29 16:05
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : __init__.py
def setup_django_env():
    import os, django

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bbc_example.settings")
    django.setup()


def check_db_connection():
    from django.db import connection

    if connection.connection:
        if not connection.is_usable():
            connection.close()