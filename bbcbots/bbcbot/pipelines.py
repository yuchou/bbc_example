# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import scrapy
from scrapy.pipelines.images import ImagesPipeline
from scrapy.pipelines.files import FilesPipeline
from scrapy.exceptions import DropItem
from example.models import Episodes, Catalogs
from datetime import datetime


class BbcbotPipeline(object):

    def process_item(self, item, spider):
        time_now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        try:
            catalog = Catalogs.objects.get(name=item['catalog'])
        except Catalogs.DoesNotExist:
            print("{} [Catalogs not exist]".format(time_now))
            return item

        ep_result = Episodes.objects.filter(episode_id=item['episode_id'], catalog=catalog.id)
        if ep_result:
            print("{} [Episode already exist]".format(time_now))
            return item
        else:
            ep_info = Episodes(
                episode_id=item['episode_id'],
                title=item['title'],
                subtitle=item['subtitle'],
                catalog=catalog,
                text=item['text'],
                link=item['link'],
                mp3_url=item['images'][0]['url'] if item['images'] else "",
                mp3_path=item['images'][0]['path'] if item['images'] else "",
                mp3_sum=item['images'][0]['checksum'] if item['images'] else "",
                pdf_url=item['files'][0]['url'] if item['files'][0] else "",
                pdf_path=item['files'][0]['path'] if item['files'][0] else "",
                pdf_sum=item['files'][0]['checksum'] if item['files'][0] else "",
                image_url=item['files'][1]['url'] if item['files'][1] else "",
                image_path=item['files'][1]['path'] if item['files'][1] else "",
                image_sum=item['files'][1]['checksum'] if item['files'][1] else ""
            )
            ep_info.save()
            print("{} [Episode saved]".format(time_now))
            return item

# class MyImagesPipeline(ImagesPipeline):
#
#     def get_media_requests(self, item, info):
#         for image_url in item['image_urls']:
#             yield scrapy.Request(image_url)
#
#     def item_completed(self, results, item, info):
#         image_paths = [x['path'] for ok, x in results if ok]
#         if not image_paths:
#             raise DropItem("Item contains no images")
#
#         return item
#
#
# class MyFilesPipeline(FilesPipeline):
#
#     def get_media_requests(self, item, info):
#         for file_url in item['file_urls']:
#             yield scrapy.Request(file_url)
#
#     def item_completed(self, results, item, info):
#         file_paths = [x['path'] for ok, x in results if ok]
#         if not file_paths:
#             raise DropItem("Item contains no files")
#
#         return item
