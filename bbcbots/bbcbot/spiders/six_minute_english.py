# -*- coding: utf-8 -*-
from bbcbots.bbcbot.items import BbcbotItem
from bbcbots.bbcbot.utils import get_episode_id
import scrapy


class A6minSpider(scrapy.Spider):
    name = 'six_minute_english'
    allowed_domains = ['www.bbc.co.uk']
    start_urls = ['http://www.bbc.co.uk/learningenglish/english/features/6-minute-english/']

    def parse(self, response):
        catalog = response.xpath('//div[@class="widget-container widget-container-left"]/div/h3/text()').extract_first()
        for href in response.xpath(
                '//div[@class="widget widget-bbcle-coursecontentlist widget-bbcle-coursecontentlist-featured widget-progress-enabled"]/div[@class="img"]/a'):
            href_link = href.xpath('./@href').extract_first()
            pic_link = href.xpath('./img/@src').extract()
            episode_id = get_episode_id(href_link)
            yield response.follow(href_link, self.parse_dir_contents,
                                  meta={'pic_link': pic_link if pic_link else "no pic link", 'catalog': catalog,
                                        'episode_id': episode_id})

        # for href in response.xpath('//ul[@class="threecol"]/*'):
        #     href_link = href.xpath('./div[@class="img"]/a/@href').extract_first()
        #     pic_link = href.xpath('./div[@class="img"]/a/img/@src').extract_first()
        #     episode_id = get_episode_id(href_link)
        #     yield response.follow(href_link, self.parse_dir_contents,
        #                           meta={'pic_link': pic_link if pic_link else "no pic link", 'catalog': catalog,
        #                                 'episode_id': episode_id})

    def parse_dir_contents(self, response):
        item = BbcbotItem()
        item['link'] = response.url
        item['image_urls'] = response.meta['pic_link']
        item['catalog'] = response.meta['catalog']
        item['episode_id'] = response.meta['episode_id']
        pdf_link = response.xpath('//a[@class="download bbcle-download-extension-pdf"]/@href').extract_first()
        mp3_link = response.xpath('//a[@class="download bbcle-download-extension-mp3"]/@href').extract_first()
        item['file_urls'] = [pdf_link, mp3_link]
        for sel in response.xpath('//div[@role="article"]/div[@class="widget-container widget-container-left"]'):
            item['title'] = sel.xpath('./div[3]/h3/text()').extract_first()
            item['subtitle'] = sel.xpath('./div[4]/div/div/*').extract_first()
            item['text'] = sel.xpath('./div[@class="widget widget-richtext 6"]/div/*').extract()
            yield item
