#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/5/18 15:01
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : utils.py


def get_episode_id(href):
    if isinstance(href, str):
        episode_id = href.split('/')[-1].split('-')[-1]
        return episode_id
    else:
        raise ValueError