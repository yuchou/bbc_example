#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/5/29 14:14
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : adminx.py
from .models import Episodes, Catalogs
from xadmin import views
import xadmin


# class BaseSetting(object):
#     enable_themes = True
#     use_bootswatch = True


class GlobalSettings(object):
    site_title = "音频数据管理后台"
    site_footer = "by yuchou"


class EpisodesAdmin(object):
    # list_display = ['episode_id', 'title', 'subtitle', 'catalog', 'text', 'link', 'mp3_url', 'mp3_path', 'mp3_sum',
    #                 'pdf_url', 'pdf_path', 'pdf_sum', 'image_url', 'image_path', 'image_sum','add_time']
    list_display  = [f.name for f in Episodes._meta.fields]
    list_filter = ('episode_id', 'title', 'subtitle')
    search_fields = ['episode_id']


class CatalogsAdmin(object):
    list_display = [f.name for f in Catalogs._meta.fields]
    # list_display = ['name', 'url', 'add_time']
    list_filter = ('name', 'link')
    search_fields = ['name']


xadmin.site.register(Episodes, EpisodesAdmin)
xadmin.site.register(Catalogs, CatalogsAdmin)
xadmin.site.register(views.CommAdminView, GlobalSettings)
# xadmin.site.register(views.BaseAdminView, BaseSetting)