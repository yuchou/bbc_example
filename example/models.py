from django.db import models
from django.utils import timezone


class Catalogs(models.Model):
    name = models.CharField(max_length=255, verbose_name='名称')
    link = models.URLField(verbose_name='网页链接')
    desc = models.CharField(max_length=255, verbose_name='描述', blank=True)
    add_time = models.DateTimeField(default=timezone.now, verbose_name='添加时间')

    class Meta:
        verbose_name = '分类列表'
        verbose_name_plural = verbose_name
        ordering = ['-add_time']

    def __str__(self):
        return '{}'.format(self.name)


class Episodes(models.Model):
    episode_id = models.CharField(max_length=255, verbose_name='编号')
    title = models.CharField(max_length=255, verbose_name='标题')
    subtitle = models.CharField(max_length=255, verbose_name='副标题')
    catalog = models.ForeignKey(Catalogs, related_name='episodes', verbose_name='类别')
    text = models.TextField(verbose_name='内容')
    link = models.URLField(verbose_name='网页链接')
    mp3_url = models.URLField(verbose_name='MP3下载链接')
    mp3_path = models.FileField(verbose_name='MP3本地路径')
    mp3_sum = models.CharField(max_length=255, verbose_name='MP3校验值')
    pdf_url = models.URLField(verbose_name='PDF下载链接')
    pdf_path = models.FileField(verbose_name='PDF本地路径')
    pdf_sum = models.CharField(max_length=255, verbose_name='PDF校验值')
    image_url = models.URLField(verbose_name='Image下载链接')
    image_path = models.ImageField(verbose_name='Image本地路径')
    image_sum = models.CharField(max_length=255, verbose_name='Image校验值')
    add_time = models.DateTimeField(default=timezone.now, verbose_name='添加时间')

    class Meta:
        verbose_name = '音频列表'
        verbose_name_plural = verbose_name
        ordering = ['-episode_id']

    def __str__(self):
        return '{}-{}-{}'.format(self.episode_id, self.title, self.catalog)