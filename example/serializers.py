#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/5/29 15:28
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : serializers.py
from rest_framework import serializers
from .models import Episodes, Catalogs


class EpisodesSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Episodes
        fields = "__all__"


class CatalogsSerializer(serializers.HyperlinkedModelSerializer):
    episodes = EpisodesSerializer(many=True)

    class Meta:
        model = Catalogs
        fields = "__all__"
