from rest_framework import viewsets
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from .models import Episodes, Catalogs
from .serializers import EpisodesSerializer, CatalogsSerializer


class EpisodesViewSet(viewsets.ModelViewSet):
    queryset = Episodes.objects.all()
    serializer_class = EpisodesSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('episode_id', 'title', 'catalog', 'link')
    search_fields = ('episode_id', 'title', 'catalog__name')
    ordering_fields = ('episode_id', 'title', 'catalog__name')


class CatalogsViewSet(viewsets.ModelViewSet):
    queryset = Catalogs.objects.all()
    serializer_class = CatalogsSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('name', 'link')
    search_fields = ('name',)
    ordering_fields = ('name',)
